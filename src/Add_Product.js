import React from 'react';
import logo from './logo.svg';
import readXlsxFile from 'read-excel-file';
import db from "./firestore.js";
import 'bootstrap/dist/css/bootstrap.css';
import * as page from './enum.js';
import Get_Data from './Get_Data.js';
import App from './App.js';

class Add_Product extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            Product : [{Prdt_name : "",Prdt_Gst : "", Prdt_Price: "",Prdt_Unit : "",Prdt_total: "",Prdt_discount:0}],
            Prod_have : this.props.prdt[0].Data[0].Cart ,
            dismiss_Dropbox : false,
            dismiss_Dropbox_Products : false,
            list_Of_Product : [],
            list_Of_Customer : [],
            search_results : [],
            search_results_Customers : [],
            Customer_name : this.props.prdt[0].Data[0].Customer_Name,
            add : this.props.prdt[0].Data[0].Customer_Add,
            city : this.props.prdt[0].Data[0].Customer_City,
            Gst : this.props.prdt[0].Data[0].Customer_Gst,
            pin : this.props.prdt[0].Data[0].Area_Pin,
            Comp_name : this.props.prdt[0].Data[0].Cust_Company_Name,
            init_ind : 0,
            init_ind_Products : 0,
            Payment : "",
            discount : "",
            Current_Id : "",
            Total_Amount : "",
            All_data : [],
        }
    }

    componentDidMount(){
        console.log("+++++++++++++++++++");
        this.Get_data_from_firestore();
        this.Get_data_from_firestore_1();
        this.Get_data_from_firestore_2();
     }

     Get_data_from_firestore_2 = () => {
        console.log("FIRESTORE 2");
        var response_2 = [];
        var uniqueNames = this.state.All_data;
         const query = db.firestore().collection("File_Data").get().then(querySnapshot => {
               console.log(`Received query snapshot of size ${querySnapshot.size}`);
               querySnapshot.docs.forEach((doc,index) => {
                    var document_id = doc.id;
                    var li_id = doc.data().status;
                    response_2.push({...doc.data(),"doc_id":document_id});
               });
               let pp1 = response_2.filter( (ele, ind) => ind === response_2.findIndex(elem => elem.doc_id === ele.doc_id));
                this.setState({
                    ...this.state,
                    All_data : pp1,
                });
        });
    }

     Get_data_from_firestore_1 = () => {
         db.firestore().collection("Customers").get().then(querySnapshot => {
//                console.log("Products",querySnapshot);
              var response_1 = [];
               querySnapshot.docs.forEach((doc,index) => {
                    var document_id = doc.id;
                    var li_data = doc.data();
                    response_1.push({...doc.data(),"doc_id":document_id});
               });
               this.setState({
                    ...this.state,
                    list_Of_Customer : response_1,
              })
        });
    }

    Get_data_from_firestore = () => {
         db.firestore().collection("Products").get().then(querySnapshot => {
//                console.log("Products",querySnapshot);
              var response = [];
               querySnapshot.docs.forEach((doc,index) => {
                    var document_id = doc.id;
                    var li_data = doc.data();
                    response.push({...doc.data(),"doc_id":document_id});
               });
               this.setState({
                    ...this.state,
                    list_Of_Product : response,
              })
        });
    }


    fetch_list_data = () => {
            return this.state.Product.map((value,index) => {
                 return (
                  <tr id={value.doc_id}>
                    <th><input value = {value.Prdt_name} name= "Prdt_name" onChange={this.NameChange.bind(this,index)}
                        placeholder="Product Name" size="10" autoComplete="new-password"/>
                    </th>
                    <th><input value = {value.Prdt_Gst} name= "Prdt_Gst" size="10" readonly autoComplete="off"/></th>
                    <th><input value = {value.Prdt_Price} name= "Prdt_Price" size="10" readonly autoComplete="off"/></th>
                    <th><input value = {value.Prdt_Unit} name= "Prdt_Unit" size="10" autoComplete="off"
                        onChange={this.Product_Unit_Change.bind(this,index)}/></th>
                    <th><input value = {value.Prdt_discount} name= "Prdt_discount" size="10" autoComplete="off"
                        onChange={this.Product_Discount_Change.bind(this,index)}/></th>
                    <th><input value = {value.Prdt_total} name= "Prdt_total" size="10" class="mb-3" readonly
                        autoComplete="off"/></th>
                  </tr>
                );
            })
    }

//    <div>
//                    <div> Product Name </div>
//                    <input value = {value.Prdt_name} name= "Prdt_name" onChange={this.NameChange.bind(this,index)}
//                        placeholder="Product Name" size="30"/>
//                    <div> Product Gst </div>
//                    <input value = {value.Prdt_Gst} name= "Prdt_Gst" size="30" readonly/>
//                    <div> Product Price </div>
//                    <input value = {value.Prdt_Price} name= "Prdt_Price" size="30" readonly/>
//                    <div> Product Unit </div>
//                    <input value = {value.Prdt_Unit} name= "Prdt_Unit" size="30"
//                        onChange={this.Product_Unit_Change.bind(this,index)}/>
//                    <div> Product Discount </div>
//                    <input value = {value.Prdt_discount} name= "Prdt_discount" size="30"
//                        onChange={this.Product_Discount_Change.bind(this,index)}/>
//                    <div> Product Price*Qty </div>
//                    <input value = {value.Prdt_total} name= "Prdt_total" size="30" class="mb-3" readonly/>
//                  </div>

    Dropbox = () => {
        var list = this.state.search_results;
        var response = []
        if(list.length > 0 && this.state.dismiss_Dropbox === false){
            list.forEach((li,index) => {
                var entry =  (
                    <div>
                        <label onClick={this.fill_Entry.bind(this,li)}>{li.Prdt_name}</label>
                    </div>
                );
                response.push(entry);
            });
        }
        return response;
    }

    fill_Entry = (data) => {
        console.log("IN FILL ENTRY");
        let index_of_list = this.state.init_ind;
        console.log("index===== ", this.state.init_ind);
        let Prdt = [...this.state.Product];
        let Pr = {...Prdt[index_of_list]};
        Pr.Prdt_name = data.Prdt_name;
        Pr.Prdt_Price = data.Prdt_Price;
        Pr.Prdt_Gst = data.Prdt_Gst;
        Pr.Prdt_Unit = 1;
        Pr.Prdt_total = data.Prdt_Price;
        Prdt[index_of_list] = Pr;
        console.log("Data from firestore is======== ",Prdt[index_of_list]);
        this.setState({
            ...this.state,
            Product : Prdt,
            dismiss_Dropbox : true,
        });
    }



    NameChange = (i,event) => {
        const { name, value } = event.target;
        console.log(value);
        var input = value.toLowerCase(); // for matching a Single word
        var names = this.state.list_Of_Product;
        console.log(names);
        var search = names.filter(entry => {
            return entry.Prdt_name.toLowerCase().includes(input);
        });
        console.log("Search===",search);
        var abc = this.state.Product;
        let Prdt = [...abc];
//        var name_1 = this.state.Customers;
        // expense[i] represent that particular value which is to be added
        Prdt[i] = {...Prdt[i],[name]:value}; // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            Product : Prdt,
            search_results : search,
            init_ind : i,
            dismiss_Dropbox : false,
            dismiss_Dropbox_Products : true,
        });
    }

    Product_Unit_Change = (i, event) => {
        const { name, value } = event.target;
        console.log("Product Unit change of index ",i," ",value);
        var abc = this.state.Product;
        console.log("Product list ",abc);
        let index_of_list = this.state.init_ind;
        let Prdt = [...abc];
        let Pr = {...Prdt[i]};
        Pr.Prdt_total = Pr.Prdt_Price * value - Pr.Prdt_discount;
        Prdt[i] = Pr;
        // expense[i] represent that particular value which is to be added
        Prdt[i] = {...Prdt[i],[name]:value}; // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            Product : Prdt,
        });
     }

     Product_Discount_Change = (i, event) => {
        const { name, value } = event.target;
        console.log("Product Unit change of index ",i," ",value);
        var abc = this.state.Product;
        console.log("Product list ",abc);
        let index_of_list = this.state.init_ind;
        let Prdt = [...abc];
        let Pr = {...Prdt[i]};
        Pr.Prdt_total = Pr.Prdt_Price * Pr.Prdt_Unit - value;
        Prdt[i] = Pr;
        // expense[i] represent that particular value which is to be added
        Prdt[i] = {...Prdt[i],[name]:value}; // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            Product : Prdt,
        });
     }

    Customer_Name_Change = (event) => {
        const { name, value } = event.target;
        console.log(value);
        var input = value.toLowerCase(); // for matching a Single word
        var names = this.state.list_Of_Customer;
        console.log(names);
        var search = names.filter(entry => {
            return entry.name.toLowerCase().includes(input);
        });
        console.log("Search===",search);
        // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            Customer_name : value,
            search_results_Customers : search,
            dismiss_Dropbox : false,
            enable_edit : true,
        });
    }

    Dropbox_Of_Customer = () => {
        var list = this.state.search_results_Customers;
        var response_1 = []
        if(list.length > 0 && this.state.dismiss_Dropbox === false){
            list.forEach((li,index) => {
                var entry =  (
                    <div>
                        <label onClick={this.fill_Entry_Cust.bind(this,li)}>{li.name}</label>
                    </div>
                );
                response_1.push(entry);
            });
        }
        return response_1;
    }

    fill_Entry_Cust = (data) => {
        this.setState({
           Customer_name : data.name,
           add : data.Add,
           Gst : data.Gst,
           Comp_name : data.Company,
           pin : data.Pin,
           city : data.City,
           dismiss_Dropbox : true,
        });
    }

    total = (Id) => {
        var discount = this.state.discount;
        var all_data = this.state.All_data;
        let sum = 0;
        let p1 = all_data.filter(it => {
            return it.doc_id === Id;
        });
        if(p1.length > 0) {
//            console.log(".................",p1);
            var abc = this.state.Product;
            var Products = p1[0].Cart;
            if(Products){
                Products.forEach(val => {
                    sum = sum + val.Prdt_total;
                });
            }
            abc.forEach(it => {
                sum = sum + it.Prdt_total;
            });
        }
        return sum - discount;
    }

    addClick = () => {
        this.setState((prevState) => ({
            Product : [...prevState.Product,{Prdt_name : "",Prdt_Gst : "", Prdt_Price: "",Prdt_Unit : "",Prdt_total: "",
                Prdt_discount : 0}],

        }));
    }

    RemoveClick = () => {
        var todos = [...this.state.Product];
        var len = todos.length;
        var new_1 = todos.slice(0,len-1);
        this.setState({
            ...this.state,
            Product : new_1,
        })
    }


    Payment_Change = (event) => {
        this.setState({
            ...this.state,
            Payment : event.target.value,
        });
    }

    Submit_Data = (Id) => {
        let total = 0;
        var Prdt = this.state.Product;
        var Cart = [];
        //var Products = this.props.Prod_have;
        var all_data = this.state.All_data;
//        console.log("All ",all_data);
        let p1 = all_data.filter(it => {
            return it.doc_id === Id;
        });
        if(p1[0].Cart){
            var Products = p1[0].Cart;
            Cart = [...Products];
        }
        Cart.push(...Prdt);
        console.log("Products Data", Prdt);
        console.log("Carts",Cart);
        var name = this.state.Customer_name;
        var add = this.state.add;
        var city = this.state.city;
        var gst = this.state.Gst;
        var Pin = this.state.pin;
        var Company = this.state.Comp_name;

        var Final_discount = this.state.discount;
        var Total = this.total(Id);
        console.log("Total ",Total);

        var doc_id = this.props.prdt[0].Id;
        var status = this.props.prdt[0].Data[0].status;
        var id = document.querySelector('#exModal');
        var id1 = document.querySelector('.modal-backdrop');
           if(id){
                console.log("========id found");
                console.dir(id);
                id.className = "modal fade";
                id.ariaModal = "false";
                id.ariaHidden = "true";
                id.style.display = "None";
                id.classList.className = "";
           }
           if(id1){
                console.log("============= id1 found");
                console.dir(id1);
                id1.className = "";
           }
        if(p1.length > 0){
            db.firestore().collection('File_Data').doc(doc_id).set({
             "Customer_Name" : name,
             "Customer_City" : city,
             "Area_Pin" : Pin,
             "Cust_Company_Name" : Company,
             "Customer_Gst" : gst,
             "Customer_Add" : add,
             "Total_Amount" : Total,
             "Cart" : Cart,
             "status" : status,
            }).then(function(){
                console.log("Data Successfully Entered from Products");
            }).catch(function(err){
                console.log(err);
        });
        }
    }

    Discount_Change = (event) => {
        this.setState({
            ...this.state,
            discount : event.target.value,
        });
    }

    Products_Of_Customer = (Id) => {
        var all_data = this.state.All_data;
//        console.log("All ",all_data);
        let p1 = all_data.filter(it => {
            return it.doc_id === Id;
        });
        if(p1.length > 0){
//            console.log("Helllooo ",p1);
            var Products = p1[0].Cart;
            var Doc_Id = p1[0].doc_id;
            if(Products) {
//                console.log("Products== ",Products);
            return Products.map((value,index) => {
                 return (
                  <tr id={Doc_Id}>
                    <th>
                    <input value = {value.Prdt_name} name= "Prdt_name" placeholder="Product Name" size="10"
                        onChange={this.Name_Unit_Shift.bind(this,index,value,Id)} autoComplete="off"/>
                    </th>
                    <th>
                    <input value = {value.Prdt_Gst} name= "Prdt_Gst" size="10" readonly/>
                    </th>
                    <th>
                    <input value = {value.Prdt_Price} name= "Prdt_Price" size="10" readonly/> </th>
                    <th>
                    <input value = {value.Prdt_Unit} name= "Prdt_Unit" size="10"
                        onChange={this.Product_Unit_Shift.bind(this,index,value,Id)}/>
                    </th>
                    <th>
                    <input value = {value.Prdt_discount} name= "Prdt_discount" size="10"
                        onChange={this.Product_Discount_Shift.bind(this,index,value,Id)}/>
                    </th>
                    <th>
                    <input value = {value.Prdt_total} name= "Prdt_total" size="10" class="mb-3" readonly/>
                    </th>
                  </tr>
                );
            });
            }
        }
    }

//    <div>
//                    <div> Product Name </div>
//                    <input value = {value.Prdt_name} name= "Prdt_name" placeholder="Product Name" size="30"
//                        onChange={this.Name_Unit_Shift.bind(this,index,value,Id)}/>
//                    <div> Product Gst </div>
//                    <input value = {value.Prdt_Gst} name= "Prdt_Gst" size="30" readonly/>
//                    <div> Product Price </div>
//                    <input value = {value.Prdt_Price} name= "Prdt_Price" size="30" readonly/>
//                    <div> Product Unit </div>
//                    <input value = {value.Prdt_Unit} name= "Prdt_Unit" size="30"
//                        onChange={this.Product_Unit_Shift.bind(this,index,value,Id)}/>
//                    <div> Product Discount </div>
//                    <input value = {value.Prdt_discount} name= "Prdt_discount" size="30"
//                        onChange={this.Product_Discount_Shift.bind(this,index,value,Id)}/>
//                    <div> Product Price*Qty </div>
//                    <input value = {value.Prdt_total} name= "Prdt_total" size="30" class="mb-3" readonly/>
//                  </div>

    Name_Unit_Shift = (i,val,Id,event) => {
        const { name, value } = event.target;
        var input = value.toLowerCase(); // for matching a Single word
        var names = this.state.list_Of_Product;
        console.log(names);
        var search = names.filter(entry => {
            return entry.Prdt_name.toLowerCase().includes(input);
        });
        var all_data = this.state.All_data;
        let p1 = all_data.filter(it => {
            return it.doc_id === Id;
        });
        let Prdt = [...p1[0].Cart];
        let Pr = {...Prdt[i]};
        Pr.Prdt_name = value;
        console.log(val);
        Prdt[i] = Pr;
        p1[0].Cart = Prdt;
        this.setState({
            ...this.state,
            All_data : p1,
            search_results : search,
            dismiss_Dropbox : true,
            dismiss_Dropbox_Products : false,
            init_ind_Products : i,
            Current_Id : Id,
        });
    }

     Dropbox_1 = () => {
        var list = this.state.search_results;
        var response = []
        if(list.length > 0 && this.state.dismiss_Dropbox_Products === false){
            list.forEach((li,index) => {
                var entry =  (
                    <div>
                        <label onClick={this.fill_Entry_1.bind(this,li)}>{li.Prdt_name}</label>
                    </div>
                );
                response.push(entry);
            });
        }
        return response;
    }

    fill_Entry_1 = (data) => {
        let index_of_list = this.state.init_ind_Products;
        var all_data = this.state.All_data;
        var Id = this.state.Current_Id;
        let p1 = all_data.filter(it => {
            return it.doc_id === Id;
        });
        var Product = this.state.Prod_have;
        let Prdt = [...p1[0].Cart];
        let Pr = {...Prdt[index_of_list]};
        Pr.Prdt_name = data.Prdt_name;
        Pr.Prdt_Price = data.Prdt_Price;
        Pr.Prdt_Gst = data.Prdt_Gst;
        Pr.Prdt_discount = 0;
        Pr.Prdt_Unit = 1;
        Pr.Prdt_total = data.Prdt_Price;
        Prdt[index_of_list] = Pr;
        p1[0].Cart = Prdt
        this.setState({
            ...this.state,
            All_data : p1,
            dismiss_Dropbox : true,
            dismiss_Dropbox_Products : true,
        });
    }

    Product_Unit_Shift = (i,val,Id,event) => {
        const { name, value } = event.target;
        var abc = this.state.Prod_have;
        var all_data = this.state.All_data;
        let p1 = all_data.filter(it => {
            return it.doc_id === Id;
        });
        let Prdt = [...p1[0].Cart];
        let Pr = {...Prdt[i]};
        Pr.Prdt_Unit = value;
        Pr.Prdt_total = Pr.Prdt_Price * value - Pr.Prdt_discount;
        Prdt[i] = Pr;
        p1[0].Cart = Prdt;
        this.setState({
            ...this.state,
            All_data : p1,
        });
     }

     Product_Discount_Shift = (i,val,Id,event) => {
        const { name, value } = event.target;
//        console.log("Product Unit change of index ",i," ",value);
        var abc = this.state.Prod_have;
        var all_data = this.state.All_data;
        let p1 = all_data.filter(it => {
            return it.doc_id === Id;
        });
        let Prdt = [...p1[0].Cart];
        let Pr = {...Prdt[i]};
        Pr.Prdt_discount = value;
        Pr.Prdt_total = Pr.Prdt_Price * Pr.Prdt_Unit - value;
        Prdt[i] = Pr;
        p1[0].Cart = Prdt;
        this.setState({
            ...this.state,
            All_data : p1,
        });
     }

    render(){
        var Prdt = this.state.Prod_have;
         var name = this.state.Customer_name;
         var add = this.state.add;
         var city = this.state.city;
         var Gst = this.state.Gst;
         var pin = this.state.pin;
         var Comp_name = this.state.Comp_name;
            Gst = this.props.prdt[0].Data[0].Customer_Gst;
            pin = this.props.prdt[0].Data[0].Area_Pin;
            Comp_name = this.props.prdt[0].Data[0].Cust_Company_Name;
            name = this.props.prdt[0].Data[0].Customer_Name;
            add = this.props.prdt[0].Data[0].Customer_Add;
            city = this.props.prdt[0].Data[0].Customer_City;
            Prdt = this.props.prdt[0].Data[0].Cart;
            var Id = this.props.prdt[0].Data[0].doc_id;
//        console.log("Product Data =====",this.props.prdt[0].Data[0]);
        return(
        <div>
            <div class="d-flex justify-content-around">
                <div>
                    <div>
                        Customer name
                    </div>
                        <input placeholder="name" class="mr-3" size="40" value={name}/>
                    <div>
                        Customer add
                    </div>
                        <input size="40" class="mr-3" value={add}/>
                    <div>
                        Customer City
                    </div>
                        <input size="40" class="mr-3" value={city}/>
                    <div>
                        Customer Gst
                    </div>
                        <input size="40" class="mr-3" value={Gst}/>
                    <div>
                        Pincode
                    </div>
                        <input size="40" class="mr-3" value={pin}/>
                        {this.Dropbox_Of_Customer()}
                </div>
                <div>
                </div>
            </div>
 <table class="table table-sm">
  <thead>
    <tr>
      <th scope="col">Product Name</th>
      <th scope="col">Product Gst</th>
      <th scope="col">product Price</th>
      <th scope="col">Product Unit</th>
      <th scope="col">Product Discount</th>
      <th scope="col">Product Price*Qty</th>
    </tr>
  </thead>
  <tbody>
        {this.Products_Of_Customer(Id)}
        {this.Dropbox_1()}
        {this.fetch_list_data()}
        {this.Dropbox()}
  </tbody>
</table>
            <button type="button" onClick={this.addClick.bind(this)}>Add Product</button>
        <button type="button" class="ml-5" onClick={this.RemoveClick.bind(this)}>Remove Product</button>
                <input class="ml-3 mb-3" placeholder="Final Discount" size="50" value={this.state.discount}
                    onChange={this.Discount_Change.bind(this)}/>
            Total : {this.total(Id)}
             <div>
                payment_mode
             </div>
             <input class="mr-4" placeholder="Payment" size="50" value={this.state.Payment}
                    onChange={this.Payment_Change.bind(this)}/>
             <button type="button" onClick={this.Submit_Data.bind(this,Id)}>Submit</button>
        </div>
        );
    }

}

export default Add_Product;
import React from 'react';
import logo from './logo.svg';
import readXlsxFile from 'read-excel-file';
import db from "./firestore.js";
import 'bootstrap/dist/css/bootstrap.css';
import * as page from './enum.js';
import Get_Data from './Get_Data.js';
import App from './App.js';

class Add_invoice extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            Product : [{Prdt_name : "",Prdt_Gst : "", Prdt_Price: "",Prdt_Unit : "",Prdt_total: "",Prdt_discount:0}],
            dismiss_Dropbox : false,
            list_Of_Product : [],
            list_Of_Customer : [],
            search_results : [],
            search_results_Customers : [],
            Customer_name : "",
            add : "",
            city : "",
            Gst : "",
            pin : "",
            Comp_name : "",
            init_ind : 0,
            Payment : "",
            discount : 0,
            total : "",
        }
    }

    componentDidMount(){
        console.log("+++++++++++++++++++");
        this.Get_data_from_firestore();
        this.Get_data_from_firestore_1();
     }

     Get_data_from_firestore_1 = () => {
         db.firestore().collection("Customers").get().then(querySnapshot => {
                console.log("Products",querySnapshot);
              var response_1 = [];
               querySnapshot.docs.forEach((doc,index) => {
                    var document_id = doc.id;
                    var li_data = doc.data();
                    response_1.push({...doc.data(),"doc_id":document_id});
               });
               this.setState({
                    ...this.state,
                    list_Of_Customer : response_1,
              })
        });
    }

    Get_data_from_firestore = () => {
         db.firestore().collection("Products").get().then(querySnapshot => {
                console.log("Products",querySnapshot);
              var response = [];
               querySnapshot.docs.forEach((doc,index) => {
                    var document_id = doc.id;
                    var li_data = doc.data();
                    response.push({...doc.data(),"doc_id":document_id});
               });
               this.setState({
                    ...this.state,
                    list_Of_Product : response,
              })
        });
    }


    fetch_list_data = () => {
            return this.state.Product.map((value,index) => {
                 return (
                  <div>
                    <div> Product Name </div>
                    <input value = {value.Prdt_name} name= "Prdt_name" onChange={this.NameChange.bind(this,index)}
                        placeholder="Product Name" size="30"/>
                    <div> Product Gst </div>
                    <input value = {value.Prdt_Gst} name= "Prdt_Gst" size="30" readonly/>
                    <div> Product Price </div>
                    <input value = {value.Prdt_Price} name= "Prdt_Price" size="30" readonly/>
                    <div> Product Unit </div>
                    <input value = {value.Prdt_Unit} name= "Prdt_Unit" size="30"
                        onChange={this.Product_Unit_Change.bind(this,index)}/>
                    <div> Product Discount </div>
                    <input value = {value.Prdt_discount} name= "Prdt_discount" size="30"
                        onChange={this.Product_Discount_Change.bind(this,index)}/>
                    <div> Product (Price*Qty-Discount) </div>
                    <input value = {value.Prdt_total} name= "Prdt_total" size="30" class="mb-3" readonly/>
                  </div>
                );
            })
    }

    Dropbox = () => {
        var list = this.state.search_results;
        var response = []
        if(list.length > 0 && this.state.dismiss_Dropbox === false){
            list.forEach((li,index) => {
                var entry =  (
                    <div>
                        <label onClick={this.fill_Entry.bind(this,li)}>{li.Prdt_name}</label>
                    </div>
                );
                response.push(entry);
            });
        }
        return response;
    }

    fill_Entry = (data) => {
        let index_of_list = this.state.init_ind;
        console.log("index===== ", this.state.init_ind);
        let Prdt = [...this.state.Product];
        let Pr = {...Prdt[index_of_list]};
        Pr.Prdt_name = data.Prdt_name;
        Pr.Prdt_Price = data.Prdt_Price;
        Pr.Prdt_Gst = data.Prdt_Gst;
        Pr.Prdt_Unit = 1;
        Pr.Prdt_total = data.Prdt_Price;
        Prdt[index_of_list] = Pr;
        console.log("Data from firestore is======== ",Prdt[index_of_list]);
        this.setState({
            ...this.state,
            Product : Prdt,
            dismiss_Dropbox : true,
        });
    }



    NameChange = (i,event) => {
        const { name, value } = event.target;
        console.log(value);
        var input = value.toLowerCase(); // for matching a Single word
        var names = this.state.list_Of_Product;
        console.log(names);
        var search = names.filter(entry => {
            return entry.Prdt_name.toLowerCase().includes(input);
        });
        console.log("Search===",search);
        var abc = this.state.Product;
        let Prdt = [...abc];
//        var name_1 = this.state.Customers;
        // expense[i] represent that particular value which is to be added
        Prdt[i] = {...Prdt[i],[name]:value}; // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            Product : Prdt,
            search_results : search,
            init_ind : i,
            dismiss_Dropbox : false,
        });
    }

    Product_Discount_Change = (i, event) => {
        const { name, value } = event.target;
        console.log("Product Unit change of index ",i," ",value);
        var abc = this.state.Product;
        console.log("Product list ",abc);
        let index_of_list = this.state.init_ind;
        let Prdt = [...abc];
        let Pr = {...Prdt[i]};
        Pr.Prdt_total = Pr.Prdt_Price * Pr.Prdt_Unit - value;
        Prdt[i] = Pr;
        // expense[i] represent that particular value which is to be added
        Prdt[i] = {...Prdt[i],[name]:value}; // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            Product : Prdt,
        });
     }

    Product_Unit_Change = (i, event) => {
        const { name, value } = event.target;
        console.log("Product Unit change of index ",i," ",value);
        var abc = this.state.Product;
        console.log("Product list ",abc);
        let index_of_list = this.state.init_ind;
        let Prdt = [...abc];
        let Pr = {...Prdt[i]};
        Pr.Prdt_total = Pr.Prdt_Price * value - Pr.Prdt_discount;
        Prdt[i] = Pr;
        // expense[i] represent that particular value which is to be added
        Prdt[i] = {...Prdt[i],[name]:value}; // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            Product : Prdt,
        });
     }

    Customer_Name_Change = (event) => {
        const { name, value } = event.target;
        console.log(value);
        var input = value.toLowerCase(); // for matching a Single word
        var names = this.state.list_Of_Customer;
        console.log(names);
        var search = names.filter(entry => {
            return entry.name.toLowerCase().includes(input);
        });
        console.log("Search===",search);
        // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            Customer_name : value,
            search_results_Customers : search,
            dismiss_Dropbox : false,
        });
    }

    Dropbox_Of_Customer = () => {
        var list = this.state.search_results_Customers;
        var response_1 = []
        if(list.length > 0 && this.state.dismiss_Dropbox === false){
            list.forEach((li,index) => {
                var entry =  (
                    <div>
                        <label onClick={this.fill_Entry_Cust.bind(this,li)}>{li.name}</label>
                    </div>
                );
                response_1.push(entry);
            });
        }
        return response_1;
    }

    fill_Entry_Cust = (data) => {
        this.setState({
           Customer_name : data.name,
           add : data.Add,
           Gst : data.Gst,
           Comp_name : data.Company,
           pin : data.Pin,
           city : data.City,
           dismiss_Dropbox : true,
        });
    }

    total = () => {
        var abc = this.state.Product;
        var discount = this.state.discount;
        let sum = 0;
        abc.forEach(it => {
            sum = sum + it.Prdt_total;
        });
        return sum - discount;
    }

    addClick = () => {
        this.setState((prevState) => ({
            Product : [...prevState.Product,{Prdt_name : "",Prdt_Gst : "", Prdt_Price: "",Prdt_Unit : "",Prdt_total: "",
                Prdt_discount : 0}]
        }));
    }

    RemoveClick = () => {
        var todos = [...this.state.Product];
        var len = todos.length;
        var new_1 = todos.slice(0,len-1);
        this.setState({
            ...this.state,
            Product : new_1,
        })
    }

    Payment_Change = (event) => {
        this.setState({
            ...this.state,
            Payment : event.target.value,
        });
    }

    Discount_Change = (event) => {
        this.setState({
            ...this.state,
            discount : event.target.value,
        });
    }

    Submit_Data = () => {
        var Prdt = this.state.Product;
        var Cart = [...Prdt];
        var name = this.state.Customer_name;
        var add = this.state.add;
        var city = this.state.city;
        var gst = this.state.Gst;
        var Pin = this.state.pin;
        var Company = this.state.Comp_name;
        var Final_discount = this.state.discount;
        var Total = this.total();
        var id = document.querySelector('#exModal_1');
        var id1 = document.querySelector('.modal-backdrop');
           if(id){
                console.log("========id found");
                console.dir(id);
                id.className = "modal fade";
                id.ariaModal = "false";
                id.ariaHidden = "true";
                id.style.display = "None";
                id.classList.className = "";
           }
           if(id1){
                console.log("============= id1 found");
                console.dir(id1);
                id1.className = "";
           }
        db.firestore().collection('File_Data').add({
             "Customer_Name" : name,
             "Customer_City" : city,
             "Area_Pin" : Pin,
             "Cust_Company_Name" : Company,
             "Customer_Gst" : gst,
             "Customer_Add" : add,
             "Cart" : Cart,
             "Total_Discount" : Final_discount,
             "Total_Amount" : Total,
             "status" : "1",
            }).then(function(){
                console.log("Data Successfully Entered from Invoice");
            }).catch(function(err){
                console.log(err);
        });
    }


    render(){
        return(
        <div>
            <div class="d-flex justify-content-around">
                <div>
                    <div>
                        Customer name
                    </div>
                        <input placeholder="name" class="mr-3" size="40" value={this.state.Customer_name}
                          onChange={this.Customer_Name_Change.bind(this)}/>
                    <div>
                        Customer add
                    </div>
                        <input size="40" class="mr-3" value={this.state.add} readonly/>
                    <div>
                        Customer City
                    </div>
                        <input size="40" class="mr-3" value={this.state.city} readonly/>
                    <div>
                        Customer Gst
                    </div>
                        <input size="40" class="mr-3" value={this.state.Gst} readonly/>
                    <div>
                        Pincode
                    </div>
                        <input size="40" class="mr-3" value={this.state.pin} readonly/>
                        {this.Dropbox_Of_Customer()}
                </div>
                <div>
                    {this.fetch_list_data()}
                    {this.Dropbox()}
                    <button type="button" onClick={this.addClick.bind(this)}>Add Product</button>
                    <button type="button" class="ml-5" onClick={this.RemoveClick.bind(this)}>Remove Product</button>
                </div>
            </div>
            <input class="mr-4 mb-3" placeholder="Final Discount" size="50" value={this.state.discount}
                    onChange={this.Discount_Change.bind(this)}/>
            Total : {this.total()}
              <div>
                payment_mode
             </div>
             <input class="mr-4" placeholder="Payment" size="50" value={this.state.Payment}
                    onChange={this.Payment_Change.bind(this)}/>
             <button type="button" onClick={this.Submit_Data.bind(this)}>Submit</button>
        </div>
        );
    }

}

export default Add_invoice;
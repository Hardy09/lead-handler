import React from 'react';
import logo from './logo.svg';
import readXlsxFile from 'read-excel-file';
import db from "./firestore.js";
import 'bootstrap/dist/css/bootstrap.css';
import * as page from './enum.js';
import Get_Data from './Get_Data.js';
import App from './App.js';
import Add_invoice from './Add_invoice.js';
const csv = require('csvtojson');

class Upload_file extends React.Component{

    constructor(props){
        super(props);
         this.state = {
            flag : page.Pages.Upload_file,
            name : "",
            email : "",
            password : "",
            ws : 1400,
            counter : 0,
            data_list : [],
        }
    }

     componentDidMount(){
        console.log("+++++++++++++++++++");
        this.Get_data_from_firestore();
     }

    Get_data_from_firestore = () => {
        console.log("FIRESTORE 1");
        var response = this.state.data_list;;
        var uniqueNames = this.state.All_data;
         const query = db.firestore().collection("File_Data").where("status","==","1").get().then(querySnapshot => {
               console.log(`Received query snapshot of size ${querySnapshot.size}`);
               querySnapshot.docs.forEach((doc,index) => {
                    var document_id = doc.id;
                    var li_id = doc.data().status;
                    response.push(li_id);
               });
                this.setState({
                    ...this.state,
                    data_list : response,
                });
        });
    }


    file_Upload_csv =  (event) => {
        event.preventDefault();
        var len = 0;
        var res = 0;
        console.dir(event.target);
        console.log(event.target.files[0]);
        var file = event.target.files[0];
        var reader = new FileReader();
        reader.readAsText(file);
        reader.onload =(r) =>{
            console.log("Printing Here Csv");
            var text = r.target.result;
            console.log("Text",text);
            csv({noheader:true,output: "csv"}).fromString(text).then((csvRow)=>{
                    len = csvRow.length;
                    csvRow.forEach( async (it) => {
//                        console.log(it);
                        this.Add_Data_to_Firestore(it);
                    });
                    console.log("CSV ROW",csvRow);
            });
        }
     }

    file_Upload_Excel = (event) => {
        console.log("Printing Here Excel");
        readXlsxFile(event.target.files[0]).then((rows) => {
            console.log(rows);
            console.log(rows.length);
        });
    }

    Add_Data_to_Firestore = (data) => {
            var li = this.state.data_list;
            li.push(1);
            db.firestore().collection('File_Data').add({
                "Customer_Name" : data[0],
                "Customer_City" : data[1],
                "Area_Pin" : data[2],
                "Cust_Company_Name" : data[3],
                "Customer_Gst" : data[4],
                "Customer_Add" : data[5],
                "status" : "1",
            }).then(function(){
                console.log("Data Successfully Entered");
//                this.setState({
//                    ...this.state,
//                    data_list : li,
//                });
            }).catch(function(err){
                console.log(err);
           });
    }

    Sign_Out = () => {
        var mythis = this;
        db.auth().signOut().then(function() {
                console.log('Signed Out');
                mythis.setState({
                    ...mythis.state,
                    flag : page.Pages.Sign_Out,
                });
        }, function(error) {
                console.error('Sign Out Error', error);
        });
    }

    Upload_btns = () => {
        var list = this.state.data_list;
        if(list.length > 0){
            return(
            <div>
                <div class="d-flex justify-content-between mb-5">
                    <div class="ml-5">Excel:<input class="ml-3"type="file" id="file_read_excel"
                        onChange={this.file_Upload_Excel.bind(this)}/></div>
                    <div>CSV:<input  class="ml-3" type="file" id="file_read_csv"
                        onChange={this.file_Upload_csv.bind(this)}/></div>
                    <div><button type="button" class="btn btn-primary mr" onClick={this.Sign_Out}>Sign Out</button></div>
                    <div><button type="button" class="btn btn-primary mr-5"
                        data-toggle="modal" data-target="#exModal_1" >Add Invoice</button></div>
                </div>
                <Get_Data />
            </div>
            );
        }return(
            <div>
                <div class="d-flex justify-content-center mb-5">
                    <div>Excel:<input class="ml-3"type="file" id="file_read_excel"
                        onChange={this.file_Upload_Excel.bind(this)}/></div>
                    <div>CSV:<input  class="ml-3" type="file" id="file_read_csv"
                        onChange={this.file_Upload_csv.bind(this)}/></div>
                    <div><button type="button" class="btn btn-primary mr" onClick={this.Sign_Out}>Sign Out</button></div>
                </div>
            </div>
        )
    }

    Login_to_upload = () => {
        var check = this.state.flag;
        if(check === page.Pages.Upload_file){
            return this.Upload_btns();
        }else if(check === page.Pages.Sign_Out){
            return <App />;
        }
    }

    render(){
        return(
            <div>
                <div class="modal fade bd-example-modal-lg" id="exModal_1" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <Add_invoice />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
                {this.Login_to_upload()}
            </div>
        );
    }


}

export default Upload_file;
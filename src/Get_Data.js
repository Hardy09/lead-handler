import React from 'react';
import logo from './logo.svg';
import readXlsxFile from 'read-excel-file';
import db from "./firestore.js";
import Add_Product from "./Add_Product.js";
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';

class Get_Data extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            All_data : [],
            data_list : [],
            delete_data : [],
            delete_data_3 : [],
            Table_2_entries : [],
            send_li : [],
            flag : false,
            Modal_flag : false,
            fetch_data : [],
            index : 0,
            mssg : "hello",
        }
    }

     componentDidMount(){
        console.log("+++++++++++++++++++");
        this.Get_data_from_firestore();
        this.Get_data_from_firestore_2();
        this.Get_data_from_firestore_3();
     }


     Get_data_from_firestore_3 = () => {
        console.log("FIRESTORE 3");
        var response_3 = [];
        var uniqueNames = this.state.All_data;
         const query = db.firestore().collection("File_Data").where("status","==","3").get().then(querySnapshot => {
               console.log(`Received query snapshot of size ${querySnapshot.size}`);
               querySnapshot.docs.forEach((doc,index) => {
                    var document_id = doc.id;
                    var li_id = doc.data().status;
                    response_3.push({...doc.data(),"doc_id":document_id});
                    uniqueNames.push({...doc.data(),"doc_id":document_id});
               });
               let pp = response_3.filter( (ele, ind) => ind === response_3.findIndex(elem => elem.doc_id === ele.doc_id));
                this.setState({
                    ...this.state,
                    delete_data_3 : pp,
                    All_data : uniqueNames,
                });
        });
    }

     Get_data_from_firestore_2 = () => {
        console.log("FIRESTORE 2");
        var response_2 = [];
        var uniqueNames = this.state.All_data;
         const query = db.firestore().collection("File_Data").where("status","==","2").get().then(querySnapshot => {
               console.log(`Received query snapshot of size ${querySnapshot.size}`);
               querySnapshot.docs.forEach((doc,index) => {
                    var document_id = doc.id;
                    var li_id = doc.data().status;
                    response_2.push({...doc.data(),"doc_id":document_id});
                    uniqueNames.push({...doc.data(),"doc_id":document_id});
               });
               let pp1 = response_2.filter( (ele, ind) => ind === response_2.findIndex(elem => elem.doc_id === ele.doc_id));
                this.setState({
                    ...this.state,
                    delete_data : pp1,
                    All_data : uniqueNames,
                });
        });
    }


    Get_data_from_firestore = () => {
        console.log("FIRESTORE 1");
        var response = [];
        var uniqueNames = this.state.All_data;
         const query = db.firestore().collection("File_Data").where("status","==","1").get().then(querySnapshot => {
               console.log(`Received query snapshot of size ${querySnapshot.size}`);
               querySnapshot.docs.forEach((doc,index) => {
                    var document_id = doc.id;
                    var li_id = doc.data().status;
                    response.push({...doc.data(),"doc_id":document_id});
                    uniqueNames.push({...doc.data(),"doc_id":document_id});
               });
               let pp = response.filter( (ele, ind) => ind === response.findIndex(elem => elem.doc_id === ele.doc_id));
                this.setState({
                    ...this.state,
                    data_list : pp,
                    All_data : uniqueNames,
                });
        });
    }

    Table_rows_1 = () => {
        var res = [];
        var data = this.state.data_list;
        data.forEach((ele,index) => {
            var b = (
                            <tr id={ele.doc_id}>
                            <td data-toggle="modal" onClick={this.Get_Modal.bind(this,ele.doc_id)}
                                data-target="#exModal">{ele.Customer_Name}</td>
                            <td data-toggle="modal" onClick={this.Get_Modal.bind(this,ele.doc_id)}
                            data-target="#exModal">{ele.Customer_City}</td>
                            <td data-toggle="modal" onClick={this.Get_Modal.bind(this,ele.doc_id)}
                            data-target="#exModal">{ele.Area_Pin}</td>
                            <td data-toggle="modal" onClick={this.Get_Modal.bind(this,ele.doc_id)}
                            data-target="#exModal">{ele.Cust_Company_Name}</td>
                            <td data-toggle="modal" onClick={this.Get_Modal.bind(this,ele.doc_id)}
                            data-target="#exModal">{ele.status}</td>
                            <td><button type="button" onClick={this.move_Me_to_2.bind(this,ele.doc_id,ele.status)}
                                class="btn btn-primary">FR</button></td>
                            </tr>
                     );
                     res.push(b);
        });
        return res;
    }

    Update_Me = (Id,val) => {
        db.firestore().collection("File_Data").doc(Id).update({
                status: val,
        }).then(function() {
            console.log("Document successfully written! Status Updated");
        }).catch(function(error) {
            console.error("Error writing document, Status not updated as: ", error);
        });
    }

    move_Me_to_2 = async (Id,data) => {
        console.log("IN move_Me_2");
        console.log(data);
        var for_me = this.state.delete_data;
        var del_from_li = this.state.data_list;
        var new_list_1 = del_from_li.filter(item =>item.doc_id !== Id);
        var new_list_2 = del_from_li.filter(item =>item.doc_id === Id);
        for_me.push(new_list_2[0]);
        this.Update_Me(Id,"2")
        this.setState({
             ...this.state,
             delete_data : for_me,
             data_list : new_list_1,
        });
//        console.log(new_list_1);
    }


    Table_rows_2 = () => {
        var list2 = [];
        var li = this.state.delete_data;
        if(li.length > 0){
            li.forEach((ele,index) => {
                let status = ele.status;
                status = 2;
            var b =(
                   <tr id={ele.doc_id}>
                              <td data-toggle="modal" onClick={this.Get_Modal.bind(this,ele.doc_id)}
                                data-target="#exModal">{ele.Customer_Name}</td>
                            <td data-toggle="modal" onClick={this.Get_Modal.bind(this,ele.doc_id)}
                            data-target="#exModal">{ele.Customer_City}</td>
                            <td data-toggle="modal" onClick={this.Get_Modal.bind(this,ele.doc_id)}
                            data-target="#exModal">{ele.Area_Pin}</td>
                            <td data-toggle="modal" onClick={this.Get_Modal.bind(this,ele.doc_id)}
                            data-target="#exModal">{ele.Cust_Company_Name}</td>
                            <td data-toggle="modal" onClick={this.Get_Modal.bind(this,ele.doc_id)}
                            data-target="#exModal">{status}</td>
                            <td><button type="button" onClick={this.move_Me_to_3.bind(this,ele.doc_id)}
                                class="btn btn-primary">FR</button></td>
                            <td><button type="button" onClick={this.move_Me_to_2_to_1.bind(this,ele.doc_id)}
                                class="btn btn-primary">BK</button></td>
                    </tr>
            );
             list2.push(b);
            });
            return list2;
        }
    }


    move_Me_to_2_to_1 = (Id) => {
        console.log("IN move_Me_2_to_1");
        var for_me = this.state.data_list;
        var del_from_li = this.state.delete_data;
        var new_list_1 = del_from_li.filter(item =>item.doc_id !== Id);
        var new_list_2 = del_from_li.filter(item =>item.doc_id === Id);
        for_me.push(new_list_2[0]);
        this.Update_Me(Id,"1");
        this.setState({
             ...this.state,
             delete_data : new_list_1,
             data_list : for_me,
        });
    }

    move_Me_to_3 = (Id) => {
        console.log("IN move_Me_1");
        var for_me = this.state.delete_data_3;
        var del_from_li = this.state.delete_data;
        var new_list_1 = del_from_li.filter(item =>item.doc_id !== Id);
        var new_list_2 = del_from_li.filter(item =>item.doc_id === Id);
        for_me.push(new_list_2[0]);
        this.Update_Me(Id,"3");
        this.setState({
             ...this.state,
             delete_data : new_list_1,
             delete_data_3 : for_me,
        });
    }

     Table_rows_3 = () => {
        var list2 = [];
        var li = this.state.delete_data_3;
        if(li.length > 0){
            li.forEach((ele,index) => {
                let status = ele.status;
                status = 3;
            var b =(
                   <tr id={ele.doc_id}>
                            <td data-toggle="modal" onClick={this.Get_Modal.bind(this,ele.doc_id)}
                                data-target="#exModal">{ele.Customer_Name}</td>
                            <td data-toggle="modal" onClick={this.Get_Modal.bind(this,ele.doc_id)}
                            data-target="#exModal">{ele.Customer_City}</td>
                            <td data-toggle="modal" onClick={this.Get_Modal.bind(this,ele.doc_id)}
                            data-target="#exModal">{ele.Area_Pin}</td>
                            <td data-toggle="modal" onClick={this.Get_Modal.bind(this,ele.doc_id)}
                            data-target="#exModal">{ele.Cust_Company_Name}</td>
                            <td data-toggle="modal" onClick={this.Get_Modal.bind(this,ele.doc_id)}
                            data-target="#exModal">{status}</td>
                            <td><button type="button" onClick={this.move_Me_to_3_to_2.bind(this,ele.doc_id)}
                                class="btn btn-primary">BK</button></td>
                    </tr>
            );
             list2.push(b);
            });
            return list2;
        }
    }

      move_Me_to_3_to_2 = (Id) => {
        console.log("IN move_Me_3_to_2");
        var for_me = this.state.delete_data;
        var del_from_li = this.state.delete_data_3;
        var new_list_1 = del_from_li.filter(item =>item.doc_id !== Id);
        var new_list_2 = del_from_li.filter(item =>item.doc_id === Id);
        for_me.push(new_list_2[0]);
        this.Update_Me(Id,"2");
        this.setState({
             ...this.state,
             delete_data : for_me,
             delete_data_3 : new_list_1,
        });
    }

    Table_Data_Status_2 = () => {
        return (
            <table class="table table-dark">
            <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">City</th>
                    <th scope="col">Pin</th>
                    <th scope="col">Company</th>
                    <th scope="col">Status</th>
                    <th scope="col">Move fr</th>
                    <th scope="col">Move bk</th>
                </tr>
            </thead>
            <tbody>
                 {this.Table_rows_2()}
            </tbody>
            </table>
        )
    }

    Get_Modal = (id) => {
            var li = this.state.fetch_data;
            console.log("IN Modal");
//            console.log(id);
            this.setState({
                ...this.state,
                index : id,
                Modal_flag : true,
            });
    }

     Table_Data_Status_1 = () => {
        return (
            <table class="table table-dark">
            <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">City</th>
                    <th scope="col">Pin</th>
                    <th scope="col">Company</th>
                    <th scope="col">Status</th>
                    <th scope="col">Move fr</th>
                </tr>
            </thead>
            <tbody>
                {this.Table_rows_1()}
            </tbody>
            </table>
        )
    }

     Table_Data_Status_3 = () => {
        return (
            <table class="table table-dark">
            <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">City</th>
                    <th scope="col">Pin</th>
                    <th scope="col">Company</th>
                    <th scope="col">Status</th>
                    <th scope="col">Move bk</th>
                </tr>
            </thead>
            <tbody>
                {this.Table_rows_3()}
            </tbody>
            </table>
        )
    }

    Modal_data = () => {
        var li = this.state.All_data;
        var flag = this.state.Modal_flag;
        let val = this.state.index;
        var map = li.filter(ele => ele.doc_id === val);
        var Product_Data = [{"Data" : map,"Id":val}]
        if(li.length > 0 && flag === true){
//            console.log("Modal Data------ ",map);
            return(
            <div>
                <div><Add_Product prdt={Product_Data}/></div>
            </div>
            );
        }
    }

    reRender = () => {
        console.log("IN RE RENDFER");
//        var id = document.querySelector('#exModal');
//        var id1 = document.querySelector('.modal-backdrop');
//           if(id){
//                console.log("========id found");
//                console.dir(id);
//                id.className = "modal fade";
//                id.ariaModal = "false";
//                id.ariaHidden = "true";
//                id.style.display = "None";
//                id.classList.className = "";
//           }
//           if(id1){
//                console.log("============= id1 found");
//                console.dir(id1);
//                id1.className = "";
//           }
        this.setState({
            ...this.state,
            Modal_flag : false,
        });
    }


      render(){
            var check = this.state.mssg;
            if(check === "hello"){
                return(
            <div>
            <div class="modal fade" id="exModal" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            {this.Modal_data()}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onClick={this.reRender.bind(this)}>Close</button>
      </div>
    </div>
  </div>
</div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col overflow-auto">
                            {this.Table_Data_Status_1()}
                    </div>
                    <div class="col overflow-auto">
                            {this.Table_Data_Status_2()}
                    </div>
                    <div class="col overflow-auto">
                            {this.Table_Data_Status_3()}
                    </div>
                </div>
            </div>
            </div>
        );
            }
    }

}

export default Get_Data;
import React from 'react';
import logo from './logo.svg';
import './App.css';
import readXlsxFile from 'read-excel-file';
import db from "./firestore.js";
import 'bootstrap/dist/css/bootstrap.css';
import * as page from './enum.js';
import Get_Data from './Get_Data.js';
import Upload_file from './Upload_file.js';
const csv = require('csvtojson');

class App extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            data_list : [],
            flag : page.Pages.Login,
            len_of_column : 0,
            email : "",
            password : "",
            ws : 1400,
        }
    }

    componentDidMount(){
        var intervalId = setInterval(() => {
            var size = window.innerWidth;
            console.log("window ",size);
            this.setState({
                ...this.state,
                ws : size,
            })
        }, 1000);
     }

    PasswordChange = (event) => {
        this.setState({
            ...this.state,
            password : event.target.value,
        });
    }

    EmailChange = (event) => {
        this.setState({
            ...this.state,
            email : event.target.value,
        });
    }

    Log_User_In = (event) => {
        event.preventDefault();
        var mythis = this;
        var email = this.state.email;
        var pass = this.state.password;
        db.auth().signInWithEmailAndPassword(email, pass).then(it =>{
            mythis.setState({
                ...mythis.state,
                flag : page.Pages.Upload_file,
            });
            console.log("login Successful");
        }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log("Error mssg",errorMessage);
        });
    }

    Login_Part = () => {
        return (
            <div>
                 <p id="welcome" className="mb-3"><strong> Welcome Back. </strong></p>
                 <button className="btn btn-success btn-lg btn-block" id="btn-color">
                 <img src="Google.png" width="60" height="50"/>Sign In with Google</button>
                 <p className="mt-4 text-center">Or, sign in with your email</p>
                 <label className="control-label mt-2 mb-3" for="Email">Email</label>
                 <input type="Email" className="form-control" placeholder="Enter Email" value= {this.state.email}
                                onChange= {this.EmailChange}/>
                 <label class="control-label mt-3 mb-3" for="password">Password</label>
                 <input type="Password" className="form-control" placeholder="Enter Password" value= {this.state.password}
                                onChange= {this.PasswordChange}/>
                 <button className="btn btn-success btn-lg btn-block mt-5" id="btn-color"
                            onClick={this.Log_User_In}>Sign In</button>
                 <div class="row">
                                <div class="col mt-3">
                                    <div><input type="radio" value="male" className="mr-2"/>Keep Me Sign In</div>
                                </div>
                                <div class="col mt-3 text-right">
                                    <div id="forgot-color"><strong>Forgot Password?</strong></div>
                                </div>
                            </div>
                 <p className="mt-4">Dont have an account? <strong id="forgot-color">Sign_Up Now</strong></p>
            </div>
        )
    }

    Hye_there = () => {
        return (
            <div class="container-fluid ">
                    <div class="row">
                        <div class="col-6" id="back_color">
                            <img src="surelocal_Img.png" width="200" height="200" class="rounded mx-auto d-block" id="Margin_me"/>
                        </div>
                        <div class="col mt-4" id="Content_margin">
                            {this.Login_Part()}
                        </div>
                    </div>
                </div>
        )
    }

    Login = () => {
        const windowSize = this.state.ws;
        if(windowSize  > 1300){
            return (
                <div>
                    {this.Hye_there()}
                </div>
            )
        }else if(windowSize > 1100){
            return(
                <div>
                    {this.Hye_there()}
                </div>
            )
        }else{
            return(
                <div class="container-fluid ">
                    <div class="row">
                        <div class="col mt-4" id="Content_margin">
                            {this.Login_Part()}
                        </div>
                    </div>
                </div>
            )
        }
    }

    Check_User_Signed_In = () => {
        var user = db.auth().currentUser;
        if (user) {
             return <Upload_file/>
        } else {
            return this.Login();
        }
    }


    Check = () => {
        var check = this.state.flag;
        if(check === page.Pages.Login){
            return this.Check_User_Signed_In();
        }else if(check === page.Pages.Upload_file){
            return <Upload_file />;
        }
    }


    render(){
        return(
            <div>
               {this.Check()}
            </div>
        );
    }


}

export default App;
